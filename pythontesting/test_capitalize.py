"""
test_capitalize.py

some sample tests
"""

import pytest


def capitalcase(string):
    """Capitalizes the thing and returns it."""
    if not isinstance(string, str):
        raise TypeError("Please provide a string argument")
    return string.capitalize()


def test_capital_case():
    """Tests a normal capital case instance."""
    assert capitalcase('testing') == 'Testing'


def test_raises_exception_on_non_string_arguments():
    """Make sure that passing in a number causes an exception."""
    with pytest.raises(TypeError):
        capitalcase(9)
