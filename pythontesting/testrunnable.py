#!/bin/python

"""
Testing some logging things
"""

import logging

logging.basicConfig(level=logging.DEBUG)

logging.warning("thing! Warning!")
logging.info("Another thing just happened")

print("well hello there")
