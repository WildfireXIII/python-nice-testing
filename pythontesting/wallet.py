#!/bin/python3
# wallet.py

# -*- coding: utf-8 -*-

"""
A thing that contains money.
"""


class InsufficientAmountError(Exception):
    """An error raised when an action tries to lower balance below 0."""

    pass


class Wallet:
    """A class that contains a certain amount of money."""

    def __init__(self, initial_amount=0):
        self.balance = initial_amount

    def spend_cash(self, amount):
        """Remove the amount of money from the wallet."""
        if self.balance < amount:
            raise InsufficientAmountError(
                "Not enough available to spend {}".format(amount)
            )
        self.balance -= amount

    def add_cash(self, amount):
        """Increment the amount of money in the wallet."""
        self.balance += amount
