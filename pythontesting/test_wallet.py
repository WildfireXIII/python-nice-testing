"""
test_wallet.py

Unit tests for this mysterious wallet thing
"""

import pytest

from wallet import InsufficientAmountError, Wallet

# --------------------------------------
#   Fixtures
# --------------------------------------


@pytest.fixture
def empty_wallet():
    """Returns a wallet instance with a zero balance."""
    return Wallet()


@pytest.fixture
def wallet():
    """Returns a wallet with a balance of 20."""
    return Wallet(20)


# --------------------------------------
#   Tests
# --------------------------------------


def test_default_initial_amount(empty_wallet):
    """Does the wallet start at 0?"""
    assert empty_wallet.balance == 0


def test_setting_initial_amount(wallet):
    """Does the initial amount correctly set the balance?"""
    assert wallet.balance == 20


def test_wallet_add_cash(wallet):
    """Does adding cash work?"""
    wallet.add_cash(80)
    assert wallet.balance == 100


def test_wallet_spend_cash(wallet):
    """Spending should decrement your balance."""
    wallet.spend_cash(10)
    assert wallet.balance == 10


def test_wallet_spend_cash_raises_exception_on_insufficient_amount(empty_wallet):
    """Spending money you don't have shouldn't work."""
    with pytest.raises(InsufficientAmountError):
        empty_wallet.spend_cash(100)


@pytest.mark.parametrize(
    "earned, spent, expected", [(30, 10, 20), (20, 2, 18), (100, 100, 0), (100, 0, 100)]
)
def test_transactions(empty_wallet, earned, spent, expected):
    """Ensure that sequences of earning and spending end up with the correct
    balance."""
    empty_wallet.add_cash(earned)
    empty_wallet.spend_cash(spent)
    assert empty_wallet.balance == expected
