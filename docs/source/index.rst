.. MyPythonTestingThing documentation master file, created by
   sphinx-quickstart on Fri Sep 28 23:14:55 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MyPythonTestingThing's documentation!
================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



Things
======

pythontesting.wallet
--------------------
.. automodule:: pythontesting.wallet
.. autoclass:: Wallet
    :members:
